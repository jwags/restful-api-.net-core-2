# RESTful .NET Core 2 Web API
This application is a C# Web API Written in Microsofts .NET Core 2 Framework. I created it following Kevin Dockx PluralSight video, Building a RESTful API with ASP.NET Core. It represents a RESTful API that would access a resource containing information about a Library.

## Resources
I used Postman to make various calls to the API. I exported all of those calls in put them in the root of the project. The file is PostmanApiCalls.json. You can import that file right into Postman and it will be a folder containing various calls to the API. URLs should be changes accordingly.

The API was built using Visual Studio 2017.


Video Notes
==============
KevinDockx Created a repo for this: https://github.com/KevinDockx/RESTfulAPIAspNetCore_Course

Standards
API Resources
+-------------+-----------------------------+-------------------------+---------------------------------+
| HTTP METHOD |       Request Payload       |       Sample URI        |        Response Payload         |
+-------------+-----------------------------+-------------------------+---------------------------------+
| GET         | -                           | /api/authors            | author collection single author |
|             |                             | /api/authors/{authorId} |                                 |
| POST        | single author               | /api/authors/           | single author                   |
| PUT         | single author               | /api/authors/{authorId} | single author or empty          |
| PATCH       | JsonPatchDocument on author | /api/authors/{authorId} | single author or empty          |
| DELETE      | -                           | /api/authors/{authorId} | -                               |
| HEAD        | -                           | /api/authors            | -                               |
|             |                             | /api/authors/{authorId} |                                 |
| OPTIONS     | -                           | /api/...                | -                               |
+-------------+-----------------------------+-------------------------+---------------------------------+

Response Codes
+---------------------+------------------------------+-----------------------------+
| Level 200 - Success | Level 400 - Client Mistakes  | Level 500 - Server Mistakes |
+---------------------+------------------------------+-----------------------------+
| 200 - OK            | 400 - Bad Request            | 500 - Internal server error |
| 201 - Created       | 401 - Unauthorized           |                             |
| 204 - No Content    | 403 - Forbidden              |                             |
|                     | 404 - Not Found              |                             |
|                     | 405 - Method not allowed     |                             |
|                     | 406 - Not acceptable         |                             |
|                     | 409 - Conflict               |                             |
|                     | 415 - Unsupported media type |                             |
|                     | 422 - Unprocessable entity   |                             |
+---------------------+------------------------------+-----------------------------+


Patch looks like this:
{
    "op : "add",
    "path" : "/a/b",
    "value" : "foo"
}
Patch can preform the following operations:
+-----------+--------+--------+-------+---------------------------------------------------------------------+
| Operation |  From  |  Path  | Value |                             Description                             |
+-----------+--------+--------+-------+---------------------------------------------------------------------+
| "add"     |        | "/a/b" | "foo" | Add foo to specified location                                       |
| "remove"  |        | "/a/b" |       | Remove the resource at specified location                           |
| "replace" |        | "/a/b" | "foo" | Replace the resource at the specified path with the specified value |
| "copy"    | "/a/b" | "/a/c" |       | Copy the resource from one location to another                      |
| "move"    | "/a/b" | "/a/c" |       | Move the resource from one location to another                      |
| "test"    |        | "/a/b" | "foo" | Tests that a value at a specified location is a certain value       |
+-----------+--------+--------+-------+---------------------------------------------------------------------+
