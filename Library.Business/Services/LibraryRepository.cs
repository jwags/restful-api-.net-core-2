﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Entities;
using Models.ContextConfiguration;

namespace Library.Business.Services
{
    public interface ILibraryRepository
    {
        IEnumerable<Author> GetAuthors();
        IEnumerable<Author> GetAuthors(IEnumerable<Guid> authorIds);
        Author GetAuthor(Guid id);
        bool AuthorExists(Guid id);
        IEnumerable<Book> GetBooksForAuthor(Guid authorId);
        Book GetBookForAuthor(Guid authorId, Guid id);
        void AddAuthor(Author author);
        void AddBookForAuthor(Guid authorId, Book book);
        bool Save();
        void DeleteBook(Book book);
        void DeleteAuthor(Author author);
        void UpdateBookForAuthor(Book book);
    }

    public class LibraryRepository : ILibraryRepository
    {
        public LibraryRepository(LibraryContext context)
        {
            this._Context = context;
        }

        private LibraryContext _Context { get; set; }

        public IEnumerable<Author> GetAuthors()
        {
            return this._Context.Authors
                           .OrderBy(author => author.FirstName)
                           .ThenBy(author => author.LastName);
        }

        public IEnumerable<Author> GetAuthors(IEnumerable<Guid> authorIds)
        {
            return this._Context.Authors.Where(a => authorIds.Contains(a.Id))
                .OrderBy(a => a.FirstName)
                .OrderBy(a => a.LastName)
                .ToList();
        }

        public Author GetAuthor(Guid id)
        {
            return this._Context.Authors
                       .FirstOrDefault(author => author.Id == id);
        }

        public bool AuthorExists(Guid id)
        {
            return this._Context.Authors
                       .Any(author => author.Id == id);
        }

        public IEnumerable<Book> GetBooksForAuthor(Guid authorId)
        {
            return this._Context.Books
                       .Where(book => book.AuthorId == authorId)
                       .OrderBy(book => book.Title);
        }

        public Book GetBookForAuthor(Guid authorId, Guid id)
        {
            return this._Context.Books
                       .FirstOrDefault(book => book.AuthorId == authorId && book.Id == id);
        }

        public void AddAuthor(Author author)
        {
            author.Id = Guid.NewGuid();
            this._Context.Authors.Add(author);

            // the repository fills the id (instead of using identity columns)
            if (author.Books.Any())
            {
                foreach (var book in author.Books)
                {
                    book.Id = Guid.NewGuid();
                }
            }
        }

        public void AddBookForAuthor(Guid authorId, Book book)
        {
            var author = GetAuthor(authorId);
            if (author != null)
            {
                // if there isn't an id filled out (ie: we're not upserting),
                // we should generate one
                if (book.Id == Guid.Empty)
                {
                    book.Id = Guid.NewGuid();
                }
                author.Books.Add(book);
            }
        }

        public bool Save()
        {
            return (this._Context.SaveChanges() >= 0);
        }

        public void DeleteBook(Book book)
        {
            this._Context.Books.Remove(book);
        }

        public void DeleteAuthor(Author author)
        {
            this._Context.Authors.Remove(author);
        }

        public void UpdateBookForAuthor(Book book)
        {
            // no code in this implementation
        }
    }
}
