﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;

namespace Library.Presentation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Program.CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseNLog()
                .UseStartup<Startup>()
                //CreateDefaultBuilder builds logger with default providers (console, debug, and eventSource)
                //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-2.2#add-providers
                //To customize this, do the following:
                .ConfigureLogging(logging =>
                                  {
                                      logging.ClearProviders();
                                      logging.AddConsole();
                                      logging.AddDebug();
                                      //NLog is a nuget package built for ASP.Net core 2
                                      logging.AddNLog();
                                  });
    }
}
