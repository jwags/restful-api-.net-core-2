using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Business.Helpers;
using Library.Business.Services;
using Library.Entities;
using Library.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library.Presentation.Controllers
{
    [Route("api/authorcollections")]
    [ApiController]
    public class AuthorCollectionsController : Controller
    {
        public AuthorCollectionsController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this._LibraryRepository = libraryRepository;
            this._Mapper = mapper;
        }

        private ILibraryRepository _LibraryRepository { get; set; }
        private IMapper _Mapper { get; set; }

        [HttpPost]
        public IActionResult CreateAuthorCollection([FromBody] IEnumerable<AuthorForCreationDto> authorCollection)
        {
            if (authorCollection == null)
            {
                return BadRequest();
            }

            var authorEntities = this._Mapper.Map<IEnumerable<Author>>(authorCollection);

            foreach (var author in authorEntities)
            {
                this._LibraryRepository.AddAuthor(author);
            }

            if(!this._LibraryRepository.Save())
            {
                throw new Exception("Creating an athor failed on save.");
            }

            var authorCollectionToReturn = this._Mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
            var idsAsString = string.Join(",", authorCollectionToReturn.Select(a => a.Id));

            return CreatedAtRoute("authorollections", new { ids = idsAsString }, authorCollectionToReturn);
        }

        [HttpGet("({ids})", Name="authorollections")]
        public IActionResult GetAuthorCollection([ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }

            var authorEntities = this._LibraryRepository.GetAuthors(ids);

            if (ids.Count() != authorEntities.Count())
            {
                return NotFound();
            }

            var authorsToReturn = this._Mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
            return Ok(authorsToReturn);
        }
    }
}
