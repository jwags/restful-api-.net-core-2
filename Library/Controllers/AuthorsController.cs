﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Library.Business.Services;
using Library.Entities;
using Library.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Library.Presentation.Controllers
{
    [Route("api/authors")]
    [ApiController]
    public class AuthorsController : Controller
    {
        public AuthorsController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this._LibraryRepository = libraryRepository;
            this._Mapper = mapper;
        }

        private ILibraryRepository _LibraryRepository { get; set; }
        private IMapper _Mapper { get; set; }

        [HttpGet()]
        public IActionResult GetAuthors()
        {
            var authorsFromRepo = this._LibraryRepository.GetAuthors();
            var authorsDto = this._Mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo);
            return this.Ok(authorsDto);
        }

        [HttpGet("{id}", Name="GetAuthor")]
        public IActionResult GetAuthor(Guid id)
        {
            var authorFromRepo = this._LibraryRepository.GetAuthor(id);
            if (authorFromRepo == null)
            {
                return this.NotFound();
            }

            var authorsDto = this._Mapper.Map<AuthorDto>(authorFromRepo);
            return Ok(authorsDto);
        }

        [HttpGet("exception")]
        public IActionResult GetException()
        {
            throw new Exception("Random exception for testing purposes.");
        }

        [HttpPost]
        public IActionResult CreateAuthor([FromBody] AuthorForCreationDto author)
        {
            if (author == null)
            {
                return BadRequest();
            }

            var authorEntity = this._Mapper.Map<Author>(author);

            this._LibraryRepository.AddAuthor(authorEntity);
            if(!this._LibraryRepository.Save())
            {
                throw new Exception("Creating an athor failed on save.");
            }

            var authorToReturn = this._Mapper.Map<AuthorDto>(authorEntity);
            return CreatedAtRoute("GetAuthor", new { id = authorToReturn.Id }, authorToReturn);
        }

        [HttpPost("{id}")]
        public IActionResult BlockAuthorCreation(Guid id)
        {
            if(this._LibraryRepository.AuthorExists(id))
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteAuthor(Guid id)
        {
            var authorFromRepo = this._LibraryRepository.GetAuthor(id);
            if(authorFromRepo == null)
            {
                return NotFound();
            }

            this._LibraryRepository.DeleteAuthor(authorFromRepo);
            if (!this._LibraryRepository.Save())
            {
                throw new Exception($"Deleting author {id} failed on save.");
            }

            return NoContent();
        }
    }
}
