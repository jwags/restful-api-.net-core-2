﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Library.Business.Services;
using Library.Entities;
using Library.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Library.Presentation.Controllers
{
    [Route("api/authors/{authorId}/books")]
    [ApiController]
    public class BooksController : Controller
    {
        public BooksController(ILibraryRepository libraryRepository, IMapper mapper, ILogger<BooksController> logger)
        {
            this._LibraryRepository = libraryRepository;
            this._Mapper = mapper;
            this._Logger = logger;
        }

        private ILibraryRepository _LibraryRepository { get; set; }
        private IMapper _Mapper { get; set; }
        private ILogger<BooksController> _Logger { get; set; }

        [HttpGet()]
        public IActionResult GetBooksForAuthor(Guid authorId)
        {
            if (!this._LibraryRepository.AuthorExists(authorId))
            {
                return this.NotFound();
            }

            var booksFromRepo = this._LibraryRepository.GetBooksForAuthor(authorId);
            var booksDto = this._Mapper.Map<IEnumerable<BookDto>>(booksFromRepo);
            return this.Ok(booksDto);
        }

        [HttpGet("{id}", Name="GetBookForAuthor")]
        public IActionResult GetBookForAuthor(Guid authorId, Guid id)
        {
            if (!this._LibraryRepository.AuthorExists(authorId))
            {
                return this.NotFound();
            }

            var bookFromRepo = this._LibraryRepository.GetBookForAuthor(authorId, id);
            if (bookFromRepo == null)
            {
                return this.NotFound();
            }

            var booksDto = this._Mapper.Map<BookDto>(bookFromRepo);
            return this.Ok(booksDto);
        }

        [HttpPost()]
        public IActionResult CreateBookForAuthor(Guid authorId, [FromBody] BookForCreationDto book)
        {
            if (book == null)
            {
                return BadRequest();
            }

            if (book.Description == book.Title)
            {
                ModelState.AddModelError(nameof(BookForCreationDto), "The provided description should be different from the title.");
            }

            // this no longer works as described in the video. The framework automatically returns a 400 when the modelstate is invalid.
            if (!ModelState.IsValid)
            {
                // return 422
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!this._LibraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookEntity = this._Mapper.Map<Book>(book);

            this._LibraryRepository.AddBookForAuthor(authorId, bookEntity);

            if(!this._LibraryRepository.Save())
            {
                throw new Exception("Creating a book failed on save.");
            }

            var bookToReturn = this._Mapper.Map<BookDto>(bookEntity);
            return CreatedAtRoute("GetBookForAuthor", new { authorId = bookToReturn.AuthorId, id = bookToReturn.Id }, bookToReturn);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteBookForAuthor(Guid authorId, Guid id)
        {
            if(!this._LibraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookForAuthorFromRepo = this._LibraryRepository.GetBookForAuthor(authorId, id);
            if(bookForAuthorFromRepo == null)
            {
                return NotFound();
            }

            this._LibraryRepository.DeleteBook(bookForAuthorFromRepo);

            if (!this._LibraryRepository.Save())
            {
                throw new Exception($"Deleting book {id} for author {authorId} failed on save.");
            }

            this._Logger.LogInformation(100, $"Book {id} for author {authorId} was deleted.");

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateBookForAuthor(Guid authorId, Guid id, [FromBody] BookForUpdateDto book)
        {
            if (book == null)
            {
                return BadRequest();
            }

            if (book.Description == book.Title)
            {
                ModelState.AddModelError(nameof(BookForUpdateDto), "The provided description should be different from the title.");
            }

            if (!this._LibraryRepository.AuthorExists(authorId))
            {
                return this.NotFound();
            }

            var bookFromRepo = this._LibraryRepository.GetBookForAuthor(authorId, id);
            if (bookFromRepo == null)
            {
                var bookToAdd = this._Mapper.Map<Book>(book);
                bookToAdd.Id = id;

                this._LibraryRepository.AddBookForAuthor(authorId, bookToAdd);

                if (!this._LibraryRepository.Save())
                {
                    throw new Exception($"Upserting book {id} for author {authorId} failed on save");
                }

                var bookToReturn = this._Mapper.Map<BookDto>(bookToAdd);
                return CreatedAtRoute("GetBookForAuthor", new { authorId = bookToReturn.AuthorId, id = bookToReturn.Id }, bookToReturn);
            }

            this._Mapper.Map(book, bookFromRepo);

            this._LibraryRepository.UpdateBookForAuthor(bookFromRepo);
            if (!this._LibraryRepository.Save())
            {
                throw new Exception($"Updating book {id} for author {authorId} failed on save.");
            }

            return NoContent();
        }

        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateBookForAuthor(Guid authorId, Guid id, [FromBody] JsonPatchDocument<BookForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!this._LibraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookForAuthorFromRepo = this._LibraryRepository.GetBookForAuthor(authorId, id);
            if (bookForAuthorFromRepo == null)
            {
                var bookDto = new BookForUpdateDto();
                patchDoc.ApplyTo(bookDto, ModelState);

                if (bookDto.Description == bookDto.Title)
                {
                    ModelState.AddModelError(nameof(BookForUpdateDto), "The provided description should be different from the title.");
                }

                TryValidateModel(bookDto);

                if(!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                var bookToAdd = this._Mapper.Map<Book>(bookDto);
                bookToAdd.Id = id;

                this._LibraryRepository.AddBookForAuthor(authorId, bookToAdd);

                if (!this._LibraryRepository.Save())
                {
                    throw new Exception($"Upserting book {id} for author {authorId} failed on save");
                }

                var bookToReturn = this._Mapper.Map<BookDto>(bookToAdd);
                return CreatedAtRoute("GetBookForAuthor", new { Id = bookToReturn.Id, AuthorId = authorId }, bookToReturn);
            }

            var bookToPatch = this._Mapper.Map<BookForUpdateDto>(bookForAuthorFromRepo);
            patchDoc.ApplyTo(bookToPatch, ModelState);

            if (bookToPatch.Description == bookToPatch.Title)
            {
                ModelState.AddModelError(nameof(BookForUpdateDto), "The provided description should be different from the title.");
            }

            TryValidateModel(bookToPatch);

            if(!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            this._Mapper.Map(bookToPatch, bookForAuthorFromRepo);
            this._LibraryRepository.UpdateBookForAuthor(bookForAuthorFromRepo);

            if (!this._LibraryRepository.Save())
            {
                throw new Exception($"Patching book {id} for author {authorId} failed on save");
            }

            return NoContent();
        }
    }
}
