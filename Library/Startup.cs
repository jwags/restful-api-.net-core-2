﻿using Library.Business.Services;
using Library.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Models.ContextConfiguration;

namespace Library.Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setupAction =>
                            {
                                setupAction.ReturnHttpNotAcceptable = true;
                                setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                                setupAction.InputFormatters.Add(new XmlDataContractSerializerInputFormatter(setupAction));
                            })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // register the DbContext on the container, getting the connection string from
            // appSettings (note: use this during development; in a production environment,
            // it's better to store the connection string in an environment variable)
            var connectionString = this.Configuration["connectionStrings:libraryDBConnectionString"];
            services.AddDbContext<LibraryContext>(o => o.UseSqlServer(connectionString));

            // register the repository
            services.AddScoped<ILibraryRepository, LibraryRepository>();

            // register automapper profiles
            AutoMapperProfile.Build(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, LibraryContext libraryContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                                        {
                                            appBuilder.Run(async context =>
                                                           {
                                                               var exctionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                                                               if (exctionHandlerFeature != null)
                                                               {
                                                                   var logger = loggerFactory.CreateLogger("Global exception logger");
                                                                   logger.LogError(500, exctionHandlerFeature.Error, exctionHandlerFeature.Error.Message);
                                                               }
                                                               context.Response.StatusCode = 500;
                                                               await context.Response.WriteAsync("Unexpected error.");
                                                           });
                                        });
            }

            app.UseStatusCodePages();
            app.UseMvc();
            libraryContext.EnsureSeedDataForContext();
        }
    }
}
