﻿using Library.Entities;
using Microsoft.EntityFrameworkCore;

namespace Models.ContextConfiguration
{
    public sealed class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
    }
}
