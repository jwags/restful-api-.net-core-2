using System.ComponentModel.DataAnnotations;
namespace Library.Models
{
    public class BookForUpdateDto : BookForMinipulationDto
    {
        [Required(ErrorMessage = "You should fill out the description.")]
        public override string Description { 
            get
            {
                return base.Description;
            }
            
            set
            {
                base.Description = value;
            }
        }
    }
}