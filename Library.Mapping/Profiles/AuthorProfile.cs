﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Library.Business.Helpers;
using Library.Entities;
using Library.Models;
using Microsoft.AspNetCore.Routing.Constraints;

namespace Library.Mapping.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            this.CreateMap<Author, AuthorDto>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(dest => dest.Age, opts => opts.MapFrom(src => src.DateOfBirth.GetCurrentAge()));

            this.CreateMap<AuthorForCreationDto, Author>();
        }
    }
}
