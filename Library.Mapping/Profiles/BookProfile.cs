﻿using AutoMapper;
using Library.Entities;
using Library.Models;

namespace Library.Mapping.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            this.CreateMap<Book, BookDto>();

            this.CreateMap<BookForCreationDto, Book>();

            this.CreateMap<BookForUpdateDto, Book>();

            this.CreateMap<Book, BookForUpdateDto>();
        }
    }
}
