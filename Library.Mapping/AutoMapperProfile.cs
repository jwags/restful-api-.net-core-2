﻿using AutoMapper;
using Library.Mapping.Profiles;
using Microsoft.Extensions.DependencyInjection;

namespace Library.Mapping
{
    public static class AutoMapperProfile
    {
        public static void Build(IServiceCollection services)
        {
            services
                .AddAutoMapper(typeof(AuthorProfile))
                .AddAutoMapper(typeof(BookProfile));
        }
    }
}
